﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurretController : MonoBehaviour
{
    private bool turretDestroyed; //Esto no entra dentro de esta practica, pero teoricametne podemos destruir la torreta.
    public TurretDetection detector; //Referencia al script de deteccion de rango
    public GameObject yawSegment; //Referencia al script de deteccion de rango
    public Transform pitchSegment; //Parte móvil de la torreta
    public float yawSpeed = 30f; //Velocidad de rotación de yaw
    public float pitchSpeed = 30f; //Velocidad de rotación de outcg
    public float yawLimit = 90f; //Límite de rotación en yaw
    public float pitchLimit = 90f; //Límite de rotación en pitch
    public Vector3 target;
    public Transform targetPlayer; //Jugador
    private float _fireTimer; 
    public float fireRate = 0.5f; //Cadencia de fuego
    public LayerMask playerDamageMask;
    //Apartado de sonido
    [Header("Sound Effects")]
    public AudioSource _AudioSource;
    public AudioClip shootSound;
    public GameObject bulletPrefab;
    public GameObject muzzlePrefab;

    //Referencias de posiciones
    public GameObject bulletStartPositionRight;
    public GameObject bulletStartPositionLeft;

    public GameObject muzleStartPositionRight;
    public GameObject muzleStartPositionLeft;

    private Quaternion yawSegmentStartRotation;
    private Quaternion pitchSegmentStartRotation;

    public void Start()
    {
        turretDestroyed = false;
        yawSegmentStartRotation = yawSegment.transform.localRotation;
        pitchSegmentStartRotation = pitchSegment.localRotation;

    }

    public void Update()
    {
        if(yawSegment == null)
        {
            turretDestroyed = true;
        }
        //Si no esta destruida la torreta
        if (!turretDestroyed)
        {
            //Comprobamos errores
            if (targetPlayer == null)
            {
            Debug.Log("You have to manually asing player targect to this turret" + transform.parent.name);
            }
            //Si detectamos al jugador
            if (detector.targetingPlayer)
            {
                //Rotamos hacia el
                Rotate();
            }

            if (_fireTimer < fireRate)
            {
                _fireTimer += Time.deltaTime;
            }
        }
        
    }

    public void Rotate()
    {
    //Codigo de rotacion hacia el jugador y que limita la rotación de la torreta
    //Esta parte del script se ha inspirado fuertemente en el asset gratuito:
    //Free turret script de la Asset Store
        float angle = 0.0f;
        Vector3 targetRelative = default(Vector3);
        Quaternion targetRotation = default(Quaternion);
        if (yawSegment && (yawLimit != 0f))
        {
            targetRelative = yawSegment.transform.InverseTransformPoint(target);
            angle = Mathf.Atan2(targetRelative.x, targetRelative.z) * Mathf.Rad2Deg;
            if (angle >= 180f) angle = 180f - angle;
            if (angle <= -180f) angle = -180f + angle;
            targetRotation = yawSegment.transform.rotation * Quaternion.Euler(0f, Mathf.Clamp(angle, -yawSpeed * Time.deltaTime, yawSpeed * Time.deltaTime), 0f);
            if ((yawLimit < 360f) && (yawLimit > 0f)) yawSegment.transform.rotation = Quaternion.RotateTowards(yawSegment.transform.parent.rotation * yawSegmentStartRotation, targetRotation, yawLimit);
            else yawSegment.transform.rotation = targetRotation;

        }
        if (pitchSegment && (pitchLimit != 0f))
        {
            targetRelative = pitchSegment.InverseTransformPoint(target);
            angle = -Mathf.Atan2(targetRelative.y, targetRelative.z) * Mathf.Rad2Deg;
            if (angle >= 180f) angle = 180f - angle;
            if (angle <= -180f) angle = -180f + angle;
            targetRotation = pitchSegment.rotation * Quaternion.Euler(Mathf.Clamp(angle, -pitchSpeed * Time.deltaTime, pitchSpeed * Time.deltaTime), 0f, 0f);
            if ((pitchLimit < 360f) && (pitchLimit > 0f)) pitchSegment.rotation = Quaternion.RotateTowards(pitchSegment.parent.rotation * pitchSegmentStartRotation, targetRotation, pitchLimit);
            else pitchSegment.rotation = targetRotation;
        }

        Debug.DrawLine(pitchSegment.position, target, Color.red);
        Debug.DrawRay(pitchSegment.position, pitchSegment.forward * (target - pitchSegment.position).magnitude, Color.green);
        Vector3 dirFromAtoB = (pitchSegment.transform.position - targetPlayer.transform.position).normalized;
        float dotProd = Vector3.Dot(dirFromAtoB, pitchSegment.transform.forward);

        if (dotProd < -0.97)
        {
            if (_fireTimer > fireRate)
            {
                Fire();
            }
        }
        else
        {
            detector.myAnimator.SetBool("Fire", false);
        }
    }

    public void Fire()
    {
        //Si hay linea de vision, isntanciamos los proyectiles y el sonido
        RaycastHit hit;
        if (Physics.Raycast(pitchSegment.position, pitchSegment.forward, out hit, 50f, playerDamageMask))
        {
            if (hit.transform.tag == "Player")
            {
                GameObject bulletRight = Instantiate(bulletPrefab, bulletStartPositionRight.transform.position, bulletStartPositionRight.transform.rotation);
                Destroy(bulletRight, 2f);
                GameObject bulletLeft = Instantiate(bulletPrefab, bulletStartPositionLeft.transform.position, bulletStartPositionLeft.transform.rotation);
                Destroy(bulletLeft, 2f);

                GameObject muzzleRight = Instantiate(muzzlePrefab, muzleStartPositionRight.transform.position, muzleStartPositionRight.transform.rotation);
                Destroy(muzzleRight, 1f);
                GameObject muzzleLeft = Instantiate(muzzlePrefab, muzleStartPositionLeft.transform.position, muzleStartPositionLeft.transform.rotation);
                Destroy(muzzleLeft, 1f);

                detector.myAnimator.CrossFadeInFixedTime("Fire", 0.01f);
                _AudioSource.PlayOneShot(shootSound);
                _fireTimer = 0.0f;
            }
        }
    }
    //Esta parte del script no es de IA
    private void LateUpdate()
    {
        if (!turretDestroyed)
        {
            if (detector.targetingPlayer)
            {
                Target(targetPlayer.position);
            }
        }
    }
    //Funcion para asignar un objetivo
    public void Target(Vector3 target)
    {
        this.target = target;
    }
}
