using UnityEngine;
using System.Collections;

[System.Serializable]
public class Turret : MonoBehaviour {

    public TurretAnimationController controller;
    public Transform yawSegment;
    public Transform pitchSegment;
    public float yawSpeed = 30f;
    public float pitchSpeed = 30f;
    public float yawLimit = 90f;
    public float pitchLimit = 90f;
    public Vector3 target;
    public Transform targetPlayer;
    private float _fireTimer;
    public float fireRate = 0.5f;
    public LayerMask playerDamageMask;
    [Header("Sound Effects")]
    public AudioSource _AudioSource;
    public AudioClip shootSound;

    private Quaternion yawSegmentStartRotation;
    private Quaternion pitchSegmentStartRotation;

    public void Start() {
        yawSegmentStartRotation = yawSegment.localRotation;
        pitchSegmentStartRotation = pitchSegment.localRotation;
    }

    public void Update() {
        if (targetPlayer == null) {
            Debug.Log("You have to manually asing player targect to this turret" + transform.parent.name);
        }
        if (controller.targetingPlayer)
        {
            Rotate();
        }

        if (_fireTimer < fireRate)
        {
            _fireTimer += Time.deltaTime;
        }
    }

    public void Rotate()
    {

        float angle = 0.0f;
        Vector3 targetRelative = default(Vector3);
        Quaternion targetRotation = default(Quaternion);
        if (yawSegment && (yawLimit != 0f))
        {
            targetRelative = yawSegment.InverseTransformPoint(target);
            angle = Mathf.Atan2(targetRelative.x, targetRelative.z) * Mathf.Rad2Deg;
            if (angle >= 180f) angle = 180f - angle;
            if (angle <= -180f) angle = -180f + angle;
            targetRotation = yawSegment.rotation * Quaternion.Euler(0f, Mathf.Clamp(angle, -yawSpeed * Time.deltaTime, yawSpeed * Time.deltaTime), 0f);
            if ((yawLimit < 360f) && (yawLimit > 0f)) yawSegment.rotation = Quaternion.RotateTowards(yawSegment.parent.rotation * yawSegmentStartRotation, targetRotation, yawLimit);
            else yawSegment.rotation = targetRotation;

        }
        if (pitchSegment && (pitchLimit != 0f))
        {
            targetRelative = pitchSegment.InverseTransformPoint(target);
            angle = -Mathf.Atan2(targetRelative.y, targetRelative.z) * Mathf.Rad2Deg;
            if (angle >= 180f) angle = 180f - angle;
            if (angle <= -180f) angle = -180f + angle;
            targetRotation = pitchSegment.rotation * Quaternion.Euler(Mathf.Clamp(angle, -pitchSpeed * Time.deltaTime, pitchSpeed * Time.deltaTime), 0f, 0f);
            if ((pitchLimit < 360f) && (pitchLimit > 0f)) pitchSegment.rotation = Quaternion.RotateTowards(pitchSegment.parent.rotation * pitchSegmentStartRotation, targetRotation, pitchLimit);
            else pitchSegment.rotation = targetRotation;
        }

        Debug.DrawLine(pitchSegment.position, target, Color.red);
        Debug.DrawRay(pitchSegment.position, pitchSegment.forward * (target - pitchSegment.position).magnitude, Color.green);
        Vector3 dirFromAtoB = (pitchSegment.transform.position - targetPlayer.transform.position).normalized;
        float dotProd = Vector3.Dot(dirFromAtoB, pitchSegment.transform.forward);
        
        

        if (dotProd < -0.97)
        {
            if (_fireTimer > fireRate)
            { 
                Fire();
            }
        }
        else
        {
            controller.myAnimator.SetBool("Fire", false);
        }
    }

    public void Fire()
    {
        RaycastHit hit;
        if (Physics.Raycast(pitchSegment.position, pitchSegment.forward, out hit, 50f, playerDamageMask))
        {
            if(hit.transform.tag == "Player") { 
                controller.myAnimator.CrossFadeInFixedTime("Fire", 0.01f);
                _AudioSource.PlayOneShot(shootSound);
                _fireTimer = 0.0f;
            }
        }
    }
    private void LateUpdate()
    {
        
        if (controller.targetingPlayer)
        {
            Target(targetPlayer.position);
        }
        
    }
    public void Target(Vector3 target){
        this.target = target;
    }

}