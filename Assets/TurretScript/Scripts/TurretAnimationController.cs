﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAnimationController : MonoBehaviour
{
    public Animator myAnimator;
    public bool targetingPlayer;
    public AudioClip turretActivate;
    public AudioClip turretDeActivate;
    public AudioSource turretAudioSource;

    // Start is called before the first frame update
    void Start()
    {
        targetingPlayer = false;
        myAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            turretAudioSource.PlayOneShot(turretActivate);
            myAnimator.SetBool("InSight", true);
            targetingPlayer = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            myAnimator.SetBool("Fire", false);
            turretAudioSource.PlayOneShot(turretDeActivate);
            myAnimator.SetBool("InSight", false);
            targetingPlayer = false;
        }
    }
}
