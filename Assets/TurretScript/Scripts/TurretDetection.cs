﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretDetection : MonoBehaviour
{
    public AudioClip turretActivate; //Audio de torreta activada
    public AudioClip turretDeActivate; //Audio de torreta desactivada
    public AudioSource turretAudioSource; //Componente audosource
    public Animator myAnimator; //Controlador de animaciones
    public bool targetingPlayer; //Booleano que nos dira si esta viendo al jugador o no


    private void OnTriggerEnter(Collider other)
    {
        //Si el jugador entra en rango
        if (other.gameObject.tag == "Player")
        {
            //Reproducimos sonido de activacion
            turretAudioSource.PlayOneShot(turretActivate);
            //Activamos la animacion
            myAnimator.SetBool("InSight", true);
            //Activamos el booleano que nos dice si esta en rango el jugador
            targetingPlayer = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //Si el jugador sale de rango
        if (other.gameObject.tag == "Player")
        {
            //Dejamos de disparar
            myAnimator.SetBool("Fire", false);
            //Reproducimos sonido de desactivación
            turretAudioSource.PlayOneShot(turretDeActivate);
            //Activamos la de esconderse
            myAnimator.SetBool("InSight", false);
            //DesActivamos el booleano que nos dice si esta en rango el jugador
            targetingPlayer = false;
        }
    }
}
