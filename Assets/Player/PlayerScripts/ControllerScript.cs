﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControllerScript : MonoBehaviour
{
    private bool win;
    private Animator anim;
    public bool isMoving;
    private CharacterController controller;
    public float speed = 6.0f;
    public float turnSpeed = 90f;
    private HealthScript healthcontroller;
    public Text BigText;
    void Start()
    {
        BigText.enabled = false;
        win = false;
        isMoving = false;
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        healthcontroller = GetComponent<HealthScript>();
    }

    void Update()
    {
        if (healthcontroller.health <= 0)
        {
            BigText.enabled = true;
            BigText.text = "YOU DIED";
            anim.Play("Death");
            Invoke("Restart", 5f);
        }
        else
        {
            anim.SetFloat("Speed", controller.velocity.magnitude);
            if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0 || Mathf.Abs(Input.GetAxis("Vertical")) > 0)
            {
                isMoving = true;
            }
            else
            {
                isMoving = false;
            }

            Vector3 movDir;
            transform.Rotate(0, Input.GetAxis("Horizontal") * turnSpeed * Time.deltaTime, 0);
            movDir = transform.forward * Input.GetAxis("Vertical") * speed;
            // moves the character in horizontal direction
            controller.Move(movDir * Time.deltaTime - Vector3.up * 0.1f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Victory")
        {
            BigText.enabled = true;
            BigText.text = "YOU WIN!";
            if (!win)
            {
                Invoke("Restart", 5f);
                win = true;
            }
            
        }
    }

    private void Restart()
    {
        SceneManager.LoadScene("SampleScene");
    }
}