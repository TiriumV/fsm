﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour
{
    public Image healtBar;
    float maxHealth = 100f;
    public float health;
    private Animator anim;

    private void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        health = maxHealth;
    }
    private void LateUpdate()
    {
        healtBar.fillAmount = health / maxHealth;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Explosion"))
        {
            health -= 60;
        }

        if (other.tag == ("Bullet"))
        {
            health -= 5;
        }


    }
}
