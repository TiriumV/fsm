﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretHealthController : MonoBehaviour
{
    [SerializeField]private float turretHealth = 100;
    public GameObject particle;
    public GameObject turretTodestroy;
    public GameObject destroyedTurretBasePrefab;
    public GameObject destroyedTurretPrefab;

    public void ApplyDamageTurret(float damage)
    {
        turretHealth -= damage;
        if (turretHealth <= 0)
        {
            turretHealth = 0f;
            GameObject TurretBase = Instantiate(destroyedTurretBasePrefab, turretTodestroy.transform.position, turretTodestroy.transform.rotation);
            GameObject TurretDestroyed = Instantiate(destroyedTurretPrefab, transform.position, transform.rotation);
            GameObject Particle = Instantiate(particle, transform.position, transform.rotation);
            Destroy(Particle, 2f);
            Destroy(turretTodestroy);
        }
    }
}
