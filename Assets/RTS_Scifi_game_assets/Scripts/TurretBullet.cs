﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBullet : MonoBehaviour
{
    public GameObject impactParticle;
    public float proyectileSpeed;

    private void Update()
    {
        transform.position += transform.forward * proyectileSpeed * Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        GameObject particle = Instantiate(impactParticle, transform.position, transform.rotation);
        Destroy(particle, 1f);
    }
}
