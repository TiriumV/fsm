﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patroller_Chase_State : MonoBehaviour
{
    public bool active;
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject explosion;
    [SerializeField] private float maxChaseDistance;
    [SerializeField] private float explosionDistance;
    private Patroller_State_Controller patrollerStateController;
    private NavMeshAgent navMeshAgent;
    private GameObject playerReference;
    private bool robotDeath;

    private void Start()
    {
        robotDeath = false;
        active = false;
        navMeshAgent = GetComponent<NavMeshAgent>();
        patrollerStateController = GetComponent<Patroller_State_Controller>();
    }

    public void Chase()
    {
        if (Immolate())
        {
            //Se detiene el movimiento, y se reporduce la animacion de explotar
            navMeshAgent.isStopped = true;
            navMeshAgent.velocity = Vector3.zero;
            anim.Play("anim_close");
            //Este booleano comprueba que solo entre una vez, para instanciar solo una explosión.
            if (robotDeath == false) 
            {
                Invoke("Explote", 0.5f);
                robotDeath = true;
            }

        }
        //Perseguir mientras esta el jugador a rango.
        if (IsPlayerInRange())
        {
            navMeshAgent.destination = playerReference.transform.position;
        }
        else //si se aleja mucho, vuele al estado patrulla.
        {
            patrollerStateController.currentState = Patroller_State_Controller.State.Patroll;
        }
    }

    private void Explote() ///Instancia una partícula de explosión que se destruye después de 4 segundos, 
                           ///también destruye el agente.
    {
        GameObject explosion_prefab = Instantiate(explosion, transform.position, transform.rotation);
        Destroy(explosion_prefab, 4f);
        Destroy(gameObject);
    }

    private bool IsPlayerInRange() 
    {
        ///Calcula la distancia al jugador y devuelve true o false, 
        ///dependiendo si el jugador esta a rango o muy lejos.
        playerReference = GameObject.FindGameObjectWithTag("Player");
        float distance = Vector3.Distance(playerReference.transform.position, transform.position);
        if (distance < maxChaseDistance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool Immolate()
    {
        ///Calcula la distancia al jugador y devuelve true o false, 
        ///dependiendo si el jugador esta lo suficientemente cerca como para explotar.
        playerReference = GameObject.FindGameObjectWithTag("Player");
        float distance = Vector3.Distance(playerReference.transform.position, transform.position);
        if (distance < explosionDistance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
