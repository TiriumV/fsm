﻿using System.Collections;
using UnityEngine;

public class Patroller_Audio_Controller : MonoBehaviour
{
    public AudioClip beepSound; //Audio clip del sonido "Beep"
    public AudioSource audisosoruce; //Audio source que reproducirá el sonido "Beep"
    public GameObject playerReference; //Referencia al player, para calcular su distancia

    public float waittime = 1;
    bool keepPlaying = true;

    private void Start()
    {
        StartCoroutine(SoundOut()); //Empezamos a reproducir el sonido al inicio del juego.
    }

    private void Update()
    {
        //Calculamos la distancia al jugador
        if (CalculateDistanceFromPlayer() < 8f)
        {
            //Si es menor que 8 metros decrementamos el tiempo entre "beeps"
            waittime = 0.2f;
        }
        else
        {
            //Si es mayor lo decrementamos
            waittime = 1f;
        }

    }
    //Esta función reproduce un sonido y espera un tiempo (waittime)
    IEnumerator SoundOut()
    {
        while (keepPlaying)
        {
            audisosoruce.PlayOneShot(beepSound);
            yield return new WaitForSeconds(waittime);
        }
    }
    //Esta función devuelve la distancia que hay con el jugador
    private float CalculateDistanceFromPlayer()
    {
        playerReference = GameObject.FindGameObjectWithTag("Player");
        float distance = Vector3.Distance(playerReference.transform.position, transform.position);
        return distance;
    }
}
