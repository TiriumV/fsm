﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patroller_Patroll_State : MonoBehaviour
{
    public bool active;
    private Patroller_State_Controller patrollerStateController;
    private NavMeshAgent navMeshAgent;
    private int currentWaypoint;
    private float waitTime;
    [Range(0.1f, 10f)]
    [SerializeField] private float waitTimeBetwenWaypoints;
    [SerializeField] private Transform vision;

    private void Start()
    {
        active = false;
        navMeshAgent = GetComponent<NavMeshAgent>();
        patrollerStateController = GetComponent<Patroller_State_Controller>();
        navMeshAgent.destination = patrollerStateController.WayPoints[0].position;
        waitTime = 0f;
        currentWaypoint = 0;
    }

    
    public bool WayPointReached()
    {
        //Devuelve true o false dependiendo de la distancia del waypoint, y si ha llegado al destino.
        //Se hacen comprobaciones adicionales para tener en cuenta bugs.
        return (navMeshAgent.remainingDistance != Mathf.Infinity &&
            navMeshAgent.pathStatus == NavMeshPathStatus.PathComplete &&
            navMeshAgent.remainingDistance < 0.5f);
    }

    public void NextWaypoint()
    {
        //Salta al siguiente waypoint, el siguiente al último será el primero.
        currentWaypoint = (currentWaypoint + 1) % patrollerStateController.WayPoints.Length;
    }

    public void MoveTowardsNextWaypoint()
    {
        //Fija destinación al siguiente waypoint
        navMeshAgent.destination = patrollerStateController.WayPoints[currentWaypoint].position;
    }

    public void Patroll()
    {
        navMeshAgent.updateRotation = true; //Actualizar la rotacion para mirar siempre en el vector forward
        CheckVision(); //Comprobar si ve al jugador
        MoveTowardsNextWaypoint(); //Ir al waypoint que le toque 

        if (WayPointReached()) //Si ha llegado empieza el tiempo de espera asignado por el jugador.
        {
            waitTime += Time.deltaTime;
        }   

        if (waitTime >= waitTimeBetwenWaypoints) //Si el tiempo de espera se ha cumplido
        {
            //Se asigna el siguiente waypoint y se resetea el tiempo de espera
            NextWaypoint();
            waitTime = 0f;
        }
    }

    ///Funcion que se encarga de la vision, si alguno de los raycasts ve al jugador,
    ///pasa inmediatamente al estado de persecución
    private void CheckVision()
    {
        
        RaycastHit hit;
        for (float i = -1; i < 1; i += 0.2f)
        {
            Vector3 rayDirection = vision.transform.forward;
            rayDirection = rayDirection + vision.TransformDirection(new Vector3(i, 0f));
            Debug.DrawRay(vision.position, rayDirection * 15f, Color.red, 0f, true);
            if (Physics.Raycast(vision.position, rayDirection, out hit, 30f)) //Lanza rayos
            {
                if (hit.transform.tag == "Player") //Si ve al jugador
                {
                    patrollerStateController.currentState = Patroller_State_Controller.State.Chase;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    { 
        if (active) //Solo funciona si este estado esta activo
        { 
            if(other.tag == "Player")
            {
                //Solo lo escucha si se esta moviendo
                if (other.gameObject.GetComponent<ControllerScript>().isMoving == true) 
                {
                    //Si lo escucha se para y pasa al estado alerta
                    patrollerStateController.currentState = Patroller_State_Controller.State.Alert;
                    navMeshAgent.isStopped = true;
                }
            }
        }
    }

    ///También necesitamos un OnTriggerStay,
    ///por si el jugador estaba quieto y se empieza a mover DENTRO del rango de audición
    private void OnTriggerStay(Collider other)
    {
        if (active) //Solo funciona si este estado esta activo
        {
            if (other.tag == "Player")
            {
                if (other.gameObject.GetComponent<ControllerScript>().isMoving == true)
                {
                    patrollerStateController.currentState = Patroller_State_Controller.State.Alert;
                    navMeshAgent.isStopped = true;
                }
            }
        }
    }
}
