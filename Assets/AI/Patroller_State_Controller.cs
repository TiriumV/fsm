﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patroller_State_Controller : MonoBehaviour
{
    [Header("References")]
    private Patroller_Patroll_State PatrollState;   //Script controlador  del estado patrulla
    private Patroller_Alert_State AlertState;       //Script controlador del estado alerta
    private Patroller_Chase_State ChaseState;       //Script controlador  del estado persecución
    [SerializeField] private Animator anim;         //Controlador de animaciones

    public Transform[] WayPoints;
    private NavMeshAgent navMeshAgent;

    public enum State
    {
        Patroll,
        Alert,
        Chase,
    }

    //Enum de estados, este enum sera llamado en los controladores de cada estado para transitar a otros estados
    public State currentState; 

    private void Start()
    {
        PatrollState = GetComponent<Patroller_Patroll_State>();
        AlertState = GetComponent<Patroller_Alert_State>();
        ChaseState = GetComponent<Patroller_Chase_State>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        currentState = State.Patroll; //El estado por defecto es la patrulla
    }

    private void Update()
    {
        ///Switch controlador de todos los estados.
        ///En vez de activar y desactivar scripts, utilizamos un booleano active que tiene cada script.
        switch (currentState)
        {
            case State.Patroll:
                PatrollState.active = true;
                AlertState.active = false;
                ChaseState.active = false;
                PatrollState.Patroll();
                break;
            case State.Alert:
                AlertState.active = true;
                PatrollState.active = false;
                ChaseState.active = false;
                AlertState.Alert();
                break;
            case State.Chase:
                ChaseState.active = true;
                AlertState.active = false;
                PatrollState.active = false;
                ChaseState.Chase();
                break;
            default:
                PatrollState.Patroll();
                break;
        }

        //Actualizar animator
        anim.SetFloat("Speed", navMeshAgent.velocity.magnitude);
    } 
}
