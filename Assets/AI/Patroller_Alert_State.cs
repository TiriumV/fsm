﻿using UnityEngine;
using UnityEngine.AI;

public class Patroller_Alert_State : MonoBehaviour
{
    public bool active;
    private Patroller_State_Controller patrollerStateController;
    private NavMeshAgent navMeshAgent;
    private bool playerLocked;
    private Transform playerTransform;
    private float alertTime;

    [Header("Customizable times")]
    [SerializeField] private float lookAtSpeed = 8; //Velocidad a la que gira hacia el jugador
    [SerializeField] private float alertMaxTime = 4; //Tiempo maximo de alerta (segundos)

    [Header("References")]
    public Transform vision; 
    Quaternion rotation;

    private void Start()
    {
        active = false;
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
        patrollerStateController = GetComponent<Patroller_State_Controller>();
        playerLocked = false;
    }

    public void Alert()
    {
        Debug.Log(playerTransform.gameObject.GetComponent<ControllerScript>().isMoving);
        Debug.Log(IsLookingAtPlayer()); 
        //Nada mas entrar en este estado empieza el contador de tiempo de alerta
        alertTime += Time.deltaTime; 
        if (alertTime >= alertMaxTime) //Si llega al tiempo maximo y no ha visto al jugador vuelve a la patrulla
        {

            navMeshAgent.isStopped = false;
            playerLocked = false;
            navMeshAgent.updateRotation = true;
            patrollerStateController.currentState = Patroller_State_Controller.State.Patroll;
        }
        else //Si no es que permanece en alerta y no se mueve.
        {
            navMeshAgent.isStopped = true;
            navMeshAgent.velocity = Vector3.zero;
        }

        // Si llega al tiempo maximo y no ha visto al jugador vuelve a la patrulla
        if (alertTime >= alertMaxTime && playerTransform.gameObject.GetComponent<ControllerScript>().isMoving == false) {
            alertTime = 0;
        }
        //Comprobamos si vemos al jugador o no
        CheckVision();
        
        if (IsLookingAtPlayer())  //Comprobamos si esta girado mirando hacia donde se ha producido el ruido (la posicion del jugador)
        {
            playerLocked = true;
            if (alertTime >= alertMaxTime && playerTransform.gameObject.GetComponent<ControllerScript>().isMoving == true)//Si lo escucha durante demasiado tiempo
            {
                //Pasamos al estado persecucion
                navMeshAgent.isStopped = false;
                patrollerStateController.currentState = Patroller_State_Controller.State.Chase;
            }
        }
        else{
            playerLocked = false;
        }

        //Si no estamos mirando hacia el jugador y este hace ruido, redirigimos la mirada al ruido
        if (!playerLocked && playerTransform.gameObject.GetComponent<ControllerScript>().isMoving == true)
        {
            //alertTime = 0f;
            rotation = Quaternion.LookRotation(playerTransform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * lookAtSpeed);
        }

    }

    private bool IsLookingAtPlayer()
    {
        //Hacemos el producto escalar para saber si estamos o no mirando al jugador
        Vector3 dirFromAtoB = (playerTransform.position - transform.position).normalized;
        float dotProd = Vector3.Dot(dirFromAtoB, transform.forward);
        if (dotProd > 0.9)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void CheckVision()
    {
        RaycastHit hit;
        //Angulo de vision con multiples raycasts
        for (float i = -1; i < 1; i += 0.2f) 
        {
            Vector3 rayDirection = vision.transform.forward;
            rayDirection = rayDirection + vision.TransformDirection
                (new Vector3(i, 0f));
            Debug.DrawRay(vision.position, rayDirection * 15f, Color.red, 0f,true);
            if (Physics.Raycast(vision.position, rayDirection, out hit, 15f /*View Sight*/))
            {
                if (hit.transform.tag == "Player") //Si vemos al jugador pasamos a perseguirlo
                {
                    navMeshAgent.isStopped = false;
                    patrollerStateController.currentState = Patroller_State_Controller.State.Chase;
                }
            }
        }
    }

    //Si el jugador sale de nuestro rango de escuchar
    private void OnTriggerExit(Collider other)
    {
        if (active)
        {
            if (other.tag == "Player")
            {
                navMeshAgent.isStopped = false;
                playerLocked = false;
                patrollerStateController.currentState = Patroller_State_Controller.State.Patroll;
            }
        }
    }
}
